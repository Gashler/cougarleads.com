<!doctype html>
<html ng-app="app">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('site.title') }}</title>
        <link href="https://fonts.googleapis.com/css?family=Oswald:300,500" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="/css/theme.css" rel="stylesheet">
        <script>
            var app_url = '{{ config('site.app_url') }}';
            var app_name = '{{ config('site.app_name') }}';
        </script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.5/angular.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular-route.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"></script>
        <script src="https://js.stripe.com/v2/"></script>
        <script>
            Stripe.setPublishableKey("{{ env('STRIPE_KEY') }}");
        </script>
        <script src="/js/app.js"></script>
        <script src="/js/routes.js"></script>
        <script src="/js/functions.js"></script>
        <?php
            function addScripts($path) {
                // include all javascript files
                $dirIterator = new RecursiveDirectoryIterator($path);
                $iterator = new RecursiveIteratorIterator(
                  $dirIterator,
                  RecursiveIteratorIterator::SELF_FIRST
                );

                foreach ($iterator as $file) {
                    if($file->getExtension() == 'js') {
                      $url = '/' . $file;
                      echo "<script src='" . $url . "'></script>\n";
                    }
                }
            }
            $paths = [
                "js/controllers",
                "components",
                "modules"
            ];
            foreach($paths as $path) {
                addScripts($path);
            }
        ?>
        @section('header_scripts')
        @show
        @include('layouts.tracking')
    </head>
    <body>
        <header id="header">
            <div class="logo">
                <a href="/"><img src="/img/cougar.svg">{{ config('site.app_name') }}</a>
            </div>
            <nav ng-if="user.role.name == 'admin'" id="header-nav">
                <li><a href="#/sites">Sites</a></li>
                <li><a href="#/users">Users</a></li>
            </nav>
            <button class="toggle-nav" onclick="toggleNav('#nav')">
                <i class="fa fa-bars"></i>
            </button>
        </header>
        <div id="phone-small-screen">
            <i class="fa fa-phone"></i> &nbsp;{{ config('site.phone') }}
        </div>
        <article ng-controller="BaseController">
            <section class="gray" id="app-menu">
                <ul class="nav nav-tabs">
                    <li ng-class="{ 'active': tab.name == 'Dashboard' }">
                        <a href="#/">Dashboard</a>
                    </li>
                    <li ng-class="{ 'active': tab.name == 'Company Profiles' }">
                        <a href="#/profiles">Companhy Profiles</a>
                    </li>
                    <li
                        ng-class="{ 'active': tab.name == 'Landing Pages' }"
                        ng-show="user.has_profile"
                    >
                        <a href="#/sites">Landing Pages</a>
                    </li>
                    <li
                        ng-class="{ 'active': tab.name == 'Campaigns' }"
                        ng-show="user.has_profile"
                    >
                        <a href="#/campaigns">Campaigns</a>
                    </li>
                    <li ng-class="{ 'active': tab.name == 'Payment' }">
                        <a href="#/payment">Payment</a>
                    </li>
                </ul>
            </section>
            <section class="padding">
                <div class="alert-main-container">
                    <div class="alert alert-main alert-@{{ message.type }}" ng-repeat="message in messages">
                        <div ng-if="message.data.length == 1">@{{ message.data[0] }}</div>
                        <ul ng-if="message.data.length > 1">
                            <li ng-repeat="submessage in message.data">@{{ submessage }}</li>
                        </ul>
                        <button class="btn btn-default pull-right" ng-click="dismissMessage($index)">Dismiss</button>
                    </div>
                </div>
                <div ng-view ng-if="baseReady && user !== undefined"></div>
            </section>
        </article>
        <footer class="inverted">
            <div class="logo">
                <img src="/img/cougar.svg" width="150"> {{ config('site.app_name') }}
            </div>
            <br>
            <div id="copyright">&copy; {{ date('Y') }} {{ config('site.app_name') }}</div>
        </footer>
        @section('footer_scripts')
        @show
    </body>
</html>
