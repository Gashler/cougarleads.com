<!doctype html>
<html ng-app="app">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('site.title') }}</title>
        <link href="https://fonts.googleapis.com/css?family=Oswald:300,500" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link href="/css/theme.css" rel="stylesheet">
        <link href="/css/site.css" rel="stylesheet">
        <script>
            var app_url = '{{ config('site.app_url') }}';
            var app_name = '{{ config('site.app_name') }}';
        </script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.5/angular.min.js"></script>
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular-route.min.js"></script> -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"></script>
        <!-- <script src="/js/app.js"></script> -->
        <script src="/js/routes.js"></script>
        <script src="/js/functions.js"></script>
        <?php
            function addScripts($path) {
                // include all javascript files
                $dirIterator = new RecursiveDirectoryIterator($path);
                $iterator = new RecursiveIteratorIterator(
                  $dirIterator,
                  RecursiveIteratorIterator::SELF_FIRST
                );

                foreach ($iterator as $file) {
                    if($file->getExtension() == 'js') {
                      $url = '/' . $file;
                      echo "<script src='" . $url . "'></script>\n";
                    }
                }
            }
            $paths = [
                "js/controllers",
                "components",
                "modules"
            ];
            foreach($paths as $path) {
                addScripts($path);
            }
        ?>
        @section('header_scripts')
        @show
        @include('layouts.tracking')
    </head>
    <body>
        <header id="header">
            <div class="logo">
                <a href="/"><img src="/img/cougar.svg">{{ config('site.app_name') }}</a>
            </div>
            <nav id="header-nav">
                <li><a href="#about">About</a></li>
                <li><a href="#services">Services</a></li>
                <li><a href="#pricing">Pricing</a></li>
                <li><a href="#contact">Contact</a></li>
                <li><a href="/login">Sign In</a></li>
                <li>
                    <i class="fa fa-phone"></i> &nbsp;{{ config('site.phone') }}
                </li>
            </nav>
            {{-- <div class="phone">
                <i class="fa fa-phone"></i> &nbsp;{{ config('site.phone') }}
            </div> --}}
            <button class="toggle-nav" onclick="toggleNav('#nav')">
                <i class="fa fa-bars"></i>
            </button>
        </header>
        <div id="phone-small-screen">
            <i class="fa fa-phone"></i> &nbsp;{{ config('site.phone') }}
        </div>
        <nav id="nav">
            <li><a href="#about">About</a></li>
            <li><a href="#services">Services</a></li>
            <li><a href="#contact">Pricing</a></li>
            <li><a href="#contact">Contact</a></li>
            <li><a href="/login">Sign In</a></li>
        </nav>
        @section('content')
        @show
        <footer class="inverted gray align-center">
            <div class="logo inline-block">
                <img src="/img/cougar.svg" width="150">
                <br>
                {{ config('site.app_name') }}
            </div>
            <br>
            <nav>
                <li><a href="#about">About</a></li>
                <li><a href="#services">Services</a></li>
                <li><a href="#pricing">Pricing</a></li>
                <li><a href="#contact">Contact</a></li>
                <li><a href="/login">Sign In</a></li>
            </nav>
            <div id="copyright">&copy; {{ date('Y') }} {{ config('site.app_name') }}</div>
        </footer>
        @section('footer_scripts')
        @show
    </body>
</html>
