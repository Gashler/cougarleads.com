<!doctype html>
<html ng-app="app">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ config('site.title') }}</title>
        <link href="https://fonts.googleapis.com/css?family=Oswald:300,500" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="/css/theme.css" rel="stylesheet">
        <link href="/css/site.css" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.5/angular.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular-route.min.js"></script>
        <script src="/js/app.js"></script>
        <script src="/js/routes.js"></script>
        <script src="/js/functions.js"></script>
        <?php
            function addScripts($path) {
                // include all javascript files
                $dirIterator = new RecursiveDirectoryIterator($path);
                $iterator = new RecursiveIteratorIterator(
                  $dirIterator,
                  RecursiveIteratorIterator::SELF_FIRST
                );

                foreach ($iterator as $file) {
                    if($file->getExtension() == 'js') {
                      $url = '/' . $file;
                      echo "<script src='" . $url . "'></script>\n";
                    }
                }
            }
            $paths = [
                "js/controllers",
                "modules"
            ];
            foreach($paths as $path) {
                addScripts($path);
            }
        ?>
        @section('header_scripts')
        @show
        @include('layouts.tracking')
    </head>
    <body>
        <header id="header">
            <div class="logo">
                <a href="/">
                    @if (isset($site->img['logo']))
                        <img src="{{ $site->img['logo']->largest }}" alt="{{ $site->name }}">
                    @else
                        {{ $site->name }}
                    @endif
                </a>
            </div>
            <div class="phone">
                <i class="fa fa-phone"></i> &nbsp;{{ $site->phone }}
            </div>
        </header>
        <div id="phone-small-screen">
            <i class="fa fa-phone"></i> &nbsp;{{ config('site.phone') }}
        </div>
        @section('content')
        @show
        <footer class="inverted gray">
            <div class="logo">
                <a href="/">
                    @if (isset($site->img['logo']))
                        <img src="{{ $site->img['logo']->md }}" alt="{{ $site->name }}">
                    @else
                        {{ $site->name }}
                    @endif
                </a>
            </div>
            <p><i class="fa fa-phone"></i> {{ $site->phone }}</p>
            <div id="copyright">Powered by <a href="{{ config('site.app_url') }}">{{ config('site.app_name') }}</a></div>
        </footer>
        @section('footer_scripts')
        @show
    </body>
</html>
