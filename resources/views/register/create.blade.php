@extends('layouts.site')
@section('content')
    <section class="padding flex justify-center gray">
        <div style="width: 100%; max-width: 400px;">
            <h1 style="font-size: 3em; margin-bottom: .5em;">Get Started for Free</h1>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h2 class="panel-title"><i class="fa fa-user"></i> Create Your Account</h2>
                </div>
                <div class="panel-body">
                    @if (Session::has('message'))
                        <div class="alert alert-success">{{ Session::get('message') }}</div>
                    @elseif (Session::has('message_caution'))
                        <div class="alert alert-caution">{{ Session::get('message_caution') }}</div>
                    @elseif (Session::has('message_danger'))
                        <div class="alert alert-danger">{{ Session::get('message_danger') }}</div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method="post" action="register">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="text" name="first_name" placeholder="First Name" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="text" name="last_name" placeholder="Last Name" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" placeholder="Email Address" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" placeholder="Password" class="form-control">
                        </div>
                        <button class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
@stop
