<img src="/img/loading.gif">
<form name="loginform" id="form" action="{{ env('HOST_URL') }}/login-redirect.php" method="post">
	@foreach ($data as $key => $value)
		<input type="hidden" name="{{ $key }}" value="{{ $value }}">
	@endforeach
</form>
<script type="text/javascript">
	document.getElementById('form').submit();
</script>
