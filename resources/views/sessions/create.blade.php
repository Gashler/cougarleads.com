@extends('layouts.site')
@section('content')
    <section class="padding gray">
        <div class="center margin-auto" style="max-width:300px">
            <form method="post" action="/sessions">
                {{ csrf_field() }}
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h1 class="panel-title">Log In</h1>
                    </div>
                    <div class="panel-body">
                        @if (Session::has('message'))
                    		<div class="alert alert-success">{{ Session::get('message') }}</div>
                    	@elseif (Session::has('message_caution'))
                    		<div class="alert alert-caution">{{ Session::get('message_caution') }}</div>
                    	@elseif (Session::has('message_danger'))
                    		<div class="alert alert-danger">{{ Session::get('message_danger') }}</div>
                    	@endif
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="email" name="email" class="form-control" placeholder="Your Email Address">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input type="password" name="password" class="form-control" placeholder="Password">
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button class='btn btn-primary'>Log In</button>
                    </div>
                </div>
            </form>
            <p><a href='/password/forgot'>Forgot Password?</a>&nbsp; | &nbsp;<a href="/register">Sign up for Free</a></p>
        </div>
    </section>
@stop
