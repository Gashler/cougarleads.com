@extends('layouts.site')
@section('header_scripts')
    <script src="/js/controllers/HomeController.js"></script>
@stop
@section('content')
    <div ng-controller="HomeController">
        <section id="hero">
            <div class="content">
                <div>
                    <h3>The Easiest Way to Get</h3>
                    <h1>Hot, High-quality Leads</h1>
                    <ul id="types">
                        <li>Your one-click, fully-automated solution for Google Adwords,<br>Facebook Ads, and Optimized Landing Pages</li>
                    </ul>
                    <br>
                    <br>
                    <a href="/register" class="button white">Get Started for Free</a>
                </div>
            </div>
        </section>
        <section id="services">
            <div class="padding align-center">
                <br>
                <br>
                <h2>How it Works</h2>
            </div>
            <div class="flex" style="justify-content: center;">
                <ol>
                    <li>
                        <h3>Get Your Free Lead Generation Landing Page</h3>
                        <div class="row">
                            <div class="col-9">
                                <ul class="checks">
                                    <li><i class="fa fa-check"></i>Simply enter your company information</li>
                                    <li><i class="fa fa-check"></i>Your landing page will be optimized for conversions</li>
                                    <li><i class="fa fa-check"></i>Free, unlimited hosting</li>
                                    <li><i class="fa fa-check"></i>No code, no hassle</li>
                                </ul>
                            </div>
                            <div class="col-3 align-right">
                                <div class="green-fade">
                                    <div class="green-layer"></div>
                                    <img src="/img/landing-page.svg" alt="Landing page">
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <h3>Set Your Weekly Advertising Budget</h3>
                        <div class="row">
                            <div class="col-9">
                                <ul class="checks">
                                    <li><i class="fa fa-check"></i>{{ config('site.app_name') }} will completely manage your advertising campaigns</li>
                                    <li><i class="fa fa-check"></i>Start small or go big; its up to you</li>
                                    <li><i class="fa fa-check"></i>No need to set up any other accounts</li>
                                    <li><i class="fa fa-check"></i>No commitment. Cancel at any time.</li>
                                </ul>
                            </div>
                            <div class="col-3 align-right">
                                <div class="green-fade">
                                    <div class="green-layer"></div>
                                    <img src="/img/expert.svg" alt="Marketing expert">
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <h3>Get Hot Leads</h3>
                        <div class="row">
                            <div class="col-9">
                                <ul class="checks">
                                    <li><i class="fa fa-check"></i>If customers are interested, start taking calls or receiving emails</li>
                                    <li><i class="fa fa-check"></i>{{ config('site.app_name') }} will analyze your results to improve conversions</li>
                                    <li><i class="fa fa-check"></i>The more data that's collected, the better the optimization</li>
                                </ul>
                            </div>
                            <div class="col-3 align-right">
                                <div class="green-fade">
                                    <div class="green-layer"></div>
                                    <img src="/img/analytics.svg" alt="Google Analytics">
                                </div>
                            </div>
                        </div>
                    </li>
                </ol>
            </div>
        </section>
        <section id="pricing" class="flex padding gray justify-center">
            <div style="max-width: 600px;">
                <h2>Pricing</h2>
                <p class="align-left">
                    <div class="green-fade pull-left" style="width: 100px; margin-right: 1.5em;">
                        <div class="green-layer"></div>
                        <img src="/img/roi.svg" alt="ROI">
                    </div>
                    A percentage of your weekly budget will cover opertating costs as a {{ config('site.app_name') }} marketing specialist develops and manages a unique strategy for your business. We hope to retain a longterm partnership, so we'll do everything we can to help you succeed at online marketing. The majority of your budget will be used to pay for ads with services such as Google Adwords and Facebook Advertising. How much you're willing to invest is entirely up to you, though we're confident that {{ config('site.app_name') }} will help you achieve the best returns.
                </p>
                <br>
                <br>
                <h2>Does it Really Work?</h2>
                <p class="align-left">
                    <div class="green-fade pull-left" style="width: 100px; margin-right: 1.5em;">
                        <div class="green-layer"></div>
                        <img src="/img/analytics-2.svg" alt="ROI" width="100">
                    </div>
                    There are many factors to a successful marketing campaign, and trying to save money by doing it all yourself often proves to be an expensive mistake. Chances are you're reading this as a direct result of one our own marketing campaigns. We wouldn't be able to make this investment if it wasn't profitable. Now let us apply the same proven strategies to your business so you can see for yourself.
                </p>
                <br>
                <br>
                <p>
                    <a href="/register" class="button white">Get Started for Free</a>
                </p>
            </div>
        </section>
        <section id="about" class="flex padding justify-center">
            <div>
                <h2>Hi, I'm Steve</h2>
                <p>
                    <div class="pull-left align-center" style="margin: 0 2em 1em 0;">
                        <img width="200" src="/img/home/stephen-gashler.jpg" style="margin-bottom: 5px;">
                        <br>
                        CEO of {{ config('site.app_name') }}</strong>
                    </div>
                    <strong>As a marketing specialist and lead engineer</strong>, I've helped many companies grow by outfitting them with the best systems, technologies, and strageties. Right now marketing is the largest industry in the world, and while it's never been easier to promote your business, with so much competition, it's never been harder to get high-quality leads.
                    <br>
                    <br>
                    <strong>Learning how to generate leads can be difficult, time-consuming, and expensive.</strong> Yet I believe that anyone with a good product or service can make it with the right strategy. Have you found your target niche and key words? Do you know how to write high-quality ads? Are you experienced with Google Adwords and Facebook Advertising? Do you know how to maximize impressions, clicks, and conversions? Is your website optimized for lead generation? Most importantly, have you seen a return on your marketing investment? If so, I wish you luck. Otherwise, that's why we created {{ config('site.app_name') }}, to take the guesswork out of online marketing.
                    <br>
                    <br>
                     <strong>Give us a try, and let's see if we can't help you drive more business.</strong> While you may not see overnight results, the more ads we run for you, the more data we'll collect, and the better we'll be able to determine the best strategy for your company. You have everything to gain, so try an experiment today.
                </p>
            </div>
        </section>
        <section id="contact" class="flex padding gray justify-center">
            <div style="max-width: 500px;">
                <h2>Questions?</h2>
                <p>Fill out the form below or call <strong>{{ config('site.phone') }}</strong> to talk to a representative.</p>
                <p>(This form is for people who are interested in learning more about our services. Please don't contact us with business offers.)</p>
                <form ng-submit="submitForm()">
                    <input ng-model="form.Name" type="text" placeholder="Your Name">
                    <input ng-model="form.Company" type="text" placeholder="Your Company Name">
                    <input ng-model="form.Position" type="text" placeholder="Your Position">
                    <input ng-model="form.Email" type="text" placeholder="Your Email Address">
                    <input ng-model="form.Phone" type="text" placeholder="Your Phone Number (optional)">
                    <textarea ng-model="form.Comments" rows="10" placeholder="Your question"></textarea>
                    <button ng-show="!submitting && !submitted" class="button">Submit</button>
                    <img class="loading" ng-show="submitting" src="/img/loading.gif">
                </form>
            </div>
        </section>
    </div>
@stop
