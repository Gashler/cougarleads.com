@extends('layouts.clients')
@section('content')
    <section>
        <div class="row">
            <div class="col-8">
                <h1>{{ $site->name }}</h1>
                <h2 style="margin-bottom: 0;">{{ $site->tagline }}</h2>
                <div class="row">
                    <div class="col-6">
                        @if (isset($site->img['photo-1']))
                            <img
                                class="full-width"
                                src="{{ $site->img['photo-1']->largest }}"
                                alt="{{ $site->img['photo-1']->pivot->text }}"
                            >
                        @endif
                    </div>
                    <div class="col-6">
                        <p>{{ $site->description }}</p>
                        <ul class="checks">
                            @foreach ($site->bullets as $bullet)
                                <li><i class="fa fa-check"></i><span class="li-content">{{ $bullet->text }}</span></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="inverted green padding">
                    @include('sites.cta')
                </div>
            </div>
        </div>
    </section>
    <section class="gray padding">
        <ul class="trust-symbols">
            @for ($x = 1; $x <= 3; $x ++)
                @if (isset($site->img['trust-symbol-' . $x]))
                    <li>
                        <img src="{{ $site->img['trust-symbol-' . $x]->largest }}">
                        <br>
                        @if (isset($site->img['trust-symbol-' . $x]->pivot->text))
                            {{ $site->img['trust-symbol-' . $x]->pivot->text }}
                        @endif
                    </li>
                @endif
            @endfor
        </ul>
    </section>
    <section>
        <div class="row">
            <div class="col-4">
                <h2>Services</h2>
                @if (isset($site->services_description))
                    <p>{{ $site->services_description }}</p>
                @endif
                <ul class="checks">
                    @foreach ($site->services as $bullet)
                        <li><i class="fa fa-check"></i><span class="li-content">{{ $bullet->text }}</span></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-3">
                <h2>Hours</h2>
                @if (isset($site->hours_description))
                    <p>{{ $site->hours_description }}</p>
                @endif
                <ul>
                    @foreach ($site->hours as $bullet)
                        <li><span class="li-content">{{ $bullet->text }}</span></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-5">
                <div class="iframe">
                    <iframe
                        width="600"
                        height="450"
                        frameborder="0" style="border:0;"
                        src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDr1JZMP-AU6kpPOnMLZjKmz_mJqF8CN54
                        &q={{ $site->address_1 }} {{ $site->address_2 }},{{ $site->city }}+{{ $site->state }}+{{ $site->zip }}" allowfullscreen>
                    </iframe>
                </div>
            </div>
        </div>
    </section>
    <section class="padding">
        <div class="inverted green padding" style="max-width: 600px; margin: 0 auto;">
            @include('sites.cta')
        </div>
    </section>
@stop
