<h2>Need service or more info?</h2>
<h3>Call <strong>{{ $site->phone }}</strong></h3>
<p>Or fill out the form below. You'll be contacted ASAP.</p>
<form method="post" action="/request-contact">
    {{ csrf_field() }}
    <div class="inpt-group">
        <span class="inpt-group-addon"><i class="fa fa-user"></i></span>
        <input name="Name" type="text" placeholder="Your Name">
    </div>
    <div class="inpt-group">
        <span class="inpt-group-addon"><i class="fa fa-envelope"></i></span>
        <input name="Email" type="text" placeholder="Your Email Address">
    </div>
    <div class="inpt-group">
        <span class="inpt-group-addon"><i class="fa fa-phone"></i></span>
        <input name="Phone" type="text" placeholder="Your Phone Number">
    </div>
    <button ng-show="!submitting && !submitted" class="button">
        <span ng-show="!package">Contact Me ASAP!</span>
        <span ng-show="package">Get Started</span>
    </button>
    <img class="loading" ng-show="submitting" src="/img/loading.gif">
</form>
