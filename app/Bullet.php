<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bullet extends Model
{
    protected $fillable = [
        'site_id',
        'text',
        'section'
    ];
}
