<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'name',
        'key',
        'email',
        'address_1',
        'address_2',
        'city',
        'state',
        'zip',
        'phone',
        'user_id',
        'website'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function sites()
    {
        return $this->hasMany(Site::class)->orderBy('created_at', 'DESC');
    }
}
