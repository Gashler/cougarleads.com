<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'role_id',
        'stripe_id',
        'card_brand',
        'card_last_four'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token'
    ];

    public static $rules = [
        'first_name' => 'required',
        'last_name' => 'required',
        'email' => 'required|email|unique:users',
        'password' => 'required',
    ];

    protected $with = [
        'role'
    ];

    protected $appends = [
        'has_profile'
    ];

    /**
    * Attributes
    */

    public function getHasProfileAttribute()
    {
        if ($this->profiles->count() > 0) {
            return true;
        }
        return false;
    }

    /**
    * Relationships
    */

    public function profiles()
    {
        return $this->hasMany(Profile::class)->orderBy('created_at', 'DESC');
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes["password"] = \Hash::make($password);
    }

    public function hasRole($key) {
        if (!is_array($key)) return false;
        foreach ($key as $role){
            if ($this->role->name === $role){
                return true;
            }
        }
        return false;
    }

    public function campaigns()
    {
        return $this->hasMany(Campaign::class)->orderBy('created_at', 'DESC');
    }
}
