<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $fillable = [
        'active',
        'amount',
        'ends_at',
        'landing_page',
        'name',
        'stripe_subscription_id',
        'stripe_plan_id',
        'user_id'
    ];
}
