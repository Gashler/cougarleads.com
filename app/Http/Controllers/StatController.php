<?php

namespace App\Http\Controllers;

use App\Profile;
use App\User;
use Illuminate\Http\Request;

class StatController extends Controller
{
    public function index()
    {
        // admins
        if (auth()->user()->hasRole(['admin'])) {
            return [
                'Sites' => Site::count(),
                'Users' => User::count()
            ];
        }

        // members
        return [
            'Company Profiles' => [
                'count' => Profile::where('user_id', auth()->user()->id)->count(),
                'path' => 'profiles/create',
                'path_index' => 'profiles',
                'singular' => 'Company Profile'
            ],
            'Active Campaigns' => [
                'count' => auth()->user()->campaigns()->count(),
                'path' => 'subscriptions/create',
                'path_index' => 'subscriptions/edit',
                'singular' => 'Campaign'
            ]
        ];
    }
}
