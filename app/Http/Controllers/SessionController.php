<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use App\Services\MailService;
use App\Http\Controllers\Controller;

class SessionController extends Controller {

    public function __construct(
        MailService $mailService
        // UserRepository $userRepo
    ) {
        // $this->userRepo = $userRepo;
        $this->mailService = $mailService;
    }

    /**
     * Show the form for creating a new session
     */
    public function create()
    {
        if(auth()->check())
        {
            return redirect('/app');
        }
        $errors[] = request()->get('message');
        return view('sessions.create', compact('errors'));
    }

    /**
     * Create a new session (log a user in)
     * POST /sessions
     *
     * @return Response
     */
    public function store($user = null)
    {
        if (\Auth::attempt([
            'email' => request('email'),
            'password' => request('password')
        ])) {
            return redirect('/app');
        } else {
            return redirect()->back()->with('message_danger', "Incorrect email or password.");
        }
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /sessions/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy()
    {
        session()->flush();
        auth()->logout();
        return redirect('/login');
    }

    // forgot password
    public function forgotPassword()
    {
        return view('sessions/password-reset');
    }

    // reset password
    public function resetPassword()
    {
        if (!$user = User::where('email', request('email'))->first()) {
            $message = "Can't find a user with the email address " . request('email');
            return redirect()->back()->with('message_danger', $message);
        }
        $user->update(['key' => str_random(40)]);
        if ($success = $this->mailService->send('reset-password', $user)) {
            $message = "An email has been sent to " . request('email') . " with instructions for logging in.";
            return redirect()->back()->with('message', $message);
        };
        return redirect()->back()->with('message_danger', $response['message']);
    }

    // reset password
    public function confirmResetPassword()
    {
        if (!$user = User::where([
            'email' => request('email'),
            'key' => request('key')
        ])->first()) {
            return "We're having trouble verifying your account. Make sure you click on the link in the most recent email that was sent to you.";
        }
        $user->update(['need_password' => 1]);
        auth()->login($user);
        return redirect('/#/password');
    }
}
