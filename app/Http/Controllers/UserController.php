<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        return User::all();
    }

    public function show($key)
    {
        $user = User::with('bullets', 'services', 'hours', 'media')->find($key);
        if (!$user) {
            $user = User::with('bullets', 'media')->where('key', $key)->first();
        }
        return $user;
    }

    public function public($key)
    {
        $user = User::with('bullets', 'services', 'hours', 'media')->where('key', $key)->first();
        session(['user' => $user]);
        $img = [];
        foreach ($user->media as $media) {
            if ($media->pivot->key !== '') {
                $img[$media->pivot->key] = $media;
            }
        }
        $user->img = $img;
        return view('users.show', compact('user'));
    }

    public function store()
    {
        return User::create();
    }

    public function update($id)
    {
        $data = request()->all();
        User::find($id)->update(request()->all());
        return response([
            'message' => [
                'type' => 'success',
                'body' => 'User updated.'
            ]
        ], 200);
    }

    public function destroy($id)
    {
        User::destroy($id);
        return response([
            'message' => [
                'type' => 'success',
                'body' => 'User deleted.'
            ]
        ], 200);
    }
}
