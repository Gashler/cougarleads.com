<?php

namespace App\Http\Controllers;

use App\Site;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function index()
    {
        if (auth()->user()->hasRole(['admin'])) {
            return Site::all();
        } else {
            return Site::whereHas('profile', function($query) {
                $query->where('user_id', auth()->user()->id);
            })->get();
        }
    }

    public function show($key)
    {
        $site = Site::with('bullets', 'services', 'hours', 'media')->find($key);
        if (!$site) {
            $site = Site::with('bullets', 'media')->where('key', $key)->first();
        }
        return $site;
    }

    public function public($key)
    {
        $site = Site::with('bullets', 'services', 'hours', 'media')->where('key', $key)->first();
        session(['site' => $site]);
        $img = [];
        foreach ($site->media as $media) {
            if ($media->pivot->key !== '') {
                $img[$media->pivot->key] = $media;
            }
        }
        $site->img = $img;
        return view('sites.show', compact('site'));
    }

    public function store()
    {
        $data = [
            'title' => 'My Landing Page'
        ];
        if (auth()->user()->hasRole(['member'])) {
            $data['profile_id'] = auth()->user()->id;
        }
        $site = Site::create($data);
        $site->key = $site->id;
        $site->update();
        return Site::find($site->id);
    }

    public function update($id)
    {
        $data = request()->all();
        Site::find($id)->update(request()->all());
        return response([
            'message' => [
                'type' => 'success',
                'body' => 'Site updated.'
            ]
        ], 200);
    }

    public function destroy($id)
    {
        Site::destroy($id);
        return response([
            'message' => [
                'type' => 'success',
                'body' => 'Site deleted.'
            ]
        ], 200);
    }
}
