<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function index()
    {
        return view('register.create');
    }

    public function store(Request $request)
    {
        $request->validate(User::$rules);
        $data = request()->all();
        $data['role_id'] = 1;
        $user = User::create($data);
        auth()->login($user);
        return redirect('/app');
    }
}
