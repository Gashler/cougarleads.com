<?php

namespace App\Http\Controllers;

use App\Site;
use App\Media;
use Illuminate\Http\Request;

class MediaController extends Controller
{

    public function index() {

        // get data
        $data = request()->all();

        // prepare query
        $query = '$media = App\Media::';
        if(isset($data['user_id'])) $query .= 'where("user_id", ' . $data["user_id"] . ')';
        if(isset($data['library'])) $query .= 'where("library", ' . $data['library'] . ')';
        if(isset($data['type'])) $query .= '->where("type", "' . $data["type"] . '")->orWhere("type", "Vector")';
        if(isset($data['start_date'])) $query .= '->where("created_at", ">=", ' . $data["start_date"] . ')';
        if(isset($data['end_date'])) $query .= '->where("created_at", "<=", ' . $data["end_date"] . ')';
        if(isset($data['take'])) $query .= '->take(' . $data["take"] . ')';
        $query .= '->get();';

        // clean up query
        $query = str_replace('::->', '::', $query);

        // evaluate query
        eval($query);

        // return data
        return $media;

    }

    /**
     * Upload media
     */
    public function store()
    {
        $data = request()->all();
        $index = 0;
        $processed_files = [];
        // pre2($data);
        foreach($data['files'] as $file) {

            // create path for files if it doesn't already exist
            $path = date('/Y/m');
            $full_path = public_path() . '/uploads/' . date('Y') . '/' . date('m');
            if (!file_exists(public_path() . '/uploads/' . date('Y') . '/' . date('m'))) {
                mkdir(public_path() . '/uploads/' . date('Y') . '/' . date('m'), 0755, true);
            }

            // add slashes for easier appending
            $path .= '/';
            $full_path .= '/';

            // get file extension
            $data['extension'] = strtolower($file->getClientOriginalExtension());

            // get filename
            $filename = basename($_FILES['files']["name"][$index]);
            $filename = explode('.', $filename);
            $filename = $filename[0];

            // if proposed url already exists, generate a new filename
            $url = $path . $filename;
            $existing_file = Media::where('url', $url)->get();
            if (count($existing_file) > 0) {
                $filename = time() . '-' . $index;
                $url = $path . $filename;
            }

            // move the uploaded file to its destination
            $uploadSuccess = $file->move($full_path, $filename . '.' . $data['extension']);

            // common media types
            $raster_image_extensions = ['jpg', 'jpeg', 'png', 'gif'];
            $vector_extensions = ['svg'];
            $image_extensions = ['svg', 'tiff', 'psd', 'ai', 'eps'];
            $video_extensions = ['avchd', 'avi', 'flv', 'mpeg', 'mpg', 'mp4', 'wmv', 'mov', 'flv', 'rm', 'vob', 'swf'];
            $audio_extensions = ['wav', 'mp3', 'wma', 'flac', 'ogg', 'ra', 'ram', 'rm', 'mid', 'aiff', 'mpa', 'm4a', 'aif', 'iff'];
            $pdf_extensions = ['pdf'];
            $document_extensions = ['doc', 'docx', 'odt', 'pages', 'rtf', 'wpd', 'wps'];
            $spreadsheet_extensions = ['gnumeric', 'gnm', 'ods', 'xls', 'xlsx', 'xlsm', 'xlsb', 'csv'];
            $text_extensions = ['txt', 'log', 'msg', 'tex'];
            $presentation_extensions = ['key', 'ppt', 'pptx', 'odp'];
            $code_extensions = ['html', 'php', 'js', 'xml', 'json', 'c', 'class', 'cpp', 'cs', 'dtd', 'fla', 'h', 'java', 'lua', 'm', 'pl', 'py', 'sh', 'sln', 'swift', 'vcxproj', 'xcodeproj'];
            $database_extensions = ['odb', 'db', 'mdb', 'accdb', 'dbf', 'pdb', 'sql'];
            $archive_extensions = ['7z', 'cbr', 'deb', 'gz', 'pkg', 'rar', 'rpm', 'sitx', 'tar.gz', 'zip', 'zipx'];

            // if media type is image
            if (in_array($data['extension'], $raster_image_extensions)) {

                // now you are able to resize the instance
                $upload_path = $full_path . $filename;
                $img = \Image::make($upload_path . '.' . $data['extension']);
                $data['width'] = $img->width();
                $data['height'] = $img->height();
                $data['size'] = $img->filesize();

                // xxs size
                if ($data['width'] >= 50) {
                    \Image::make($upload_path . '.' . $data['extension'])->resize(null, 50, function($constraint) {
                        $constraint->aspectRatio();
                    })->save($upload_path . '-xxs.' . $data['extension']);
                }

                // md size
                if ($data['width'] >= 100) {
                    \Image::make($upload_path . '.' . $data['extension'])->resize(null, 100, function($constraint) {
                        $constraint->aspectRatio();
                    })->save($upload_path . '-xs.' . $data['extension']);
                }

                // md size
                if ($data['width'] >= 200) {
                    \Image::make($upload_path . '.' . $data['extension'])->resize(null, 200, function($constraint) {
                        $constraint->aspectRatio();
                    })->save($upload_path . '-sm.' . $data['extension']);
                }

                // md size
                if ($data['width'] >= 400) {
                    \Image::make($upload_path . '.' . $data['extension'])->resize(null, 400, function($constraint) {
                        $constraint->aspectRatio();
                    })->save($upload_path . '-md.' . $data['extension']);
                }

                // lg size
                if ($data['width'] >= 800) {
                    \Image::make($upload_path . '.' . $data['extension'])->resize(null, 800, function($constraint) {
                        $constraint->aspectRatio();
                    })->save($upload_path . '-lg.' . $data['extension']);
                }

                // xl size
                if ($data['width'] >= 1200) {
                    \Image::make($upload_path . '.' . $data['extension'])->fit(1200, 1200)->save($upload_path . '-xl.' . $data['extension']);
                }

                $data['type'] = 'Image';

            }

            // assign other media types
            elseif (in_array($data['extension'], $vector_extensions)) $data['type'] = 'Vector';
            elseif (in_array($data['extension'], $image_extensions)) $data['type'] = 'Image media';
            elseif (in_array($data['extension'], $video_extensions)) $data['type'] = 'Video';
            elseif (in_array($data['extension'], $audio_extensions)) $data['type'] = 'Audio';
            elseif (in_array($data['extension'], $pdf_extensions)) $data['type'] = 'PDF';
            elseif (in_array($data['extension'], $document_extensions)) $data['type'] = 'Document';
            elseif (in_array($data['extension'], $spreadsheet_extensions)) $data['type'] = 'Spreadsheet';
            elseif (in_array($data['extension'], $text_extensions)) $data['type'] = 'Text';
            elseif (in_array($data['extension'], $presentation_extensions)) $data['type'] = 'Presentation';
            elseif (in_array($data['extension'], $code_extensions)) $data['type'] = 'Code';
            elseif (in_array($data['extension'], $database_extensions)) $data['type'] = 'Database';
            elseif (in_array($data['extension'], $archive_extensions)) $data['type'] = 'Archive';
            else $data['type'] = 'File';

            // assign values to data array
            $file_number = $index + 1;
            if ($index < 10) $file_number = '00' . $file_number;
            elseif ($index < 100) $file_number = '0' . $file_number;
            $processed_files[$index]['url'] = $url;
            $processed_files[$index]['type'] = $data['type'];
            if (isset($data['height'])) {
                $processed_files[$index]['height'] = $data['height'];
                $processed_files[$index]['width'] = $data['width'];
                $processed_files[$index]['size'] = $data['size'];
            }
            $processed_files[$index]['extension'] = $data['extension'];
            $processed_files[$index]['library'] = $data['library'];
            if (isset($data['title'])) {
                if ($data['title'] == '') {
                    $processed_files[$index]['title'] = $filename . '.' . $data['extension'];
                }
                else $processed_files[$index]['title'] = $data['title'] . ' ' . $file_number;
            }
            if (isset($data['description'])) $processed_files[$index]['description'] = $data['description'];
            $index ++;
        }

        // store in db and redirect
        $media = [];
        foreach($processed_files as $processed_file) {
            $m = Media::create($processed_file);
            $media[] = $m;

            // attach media
            if(isset($data['model']) && isset($data['id'])) {
                $model = $data['model']::find($data['id']);
                $model->media()->save($m);
            }

            // store tags
            if (isset($data['tags'])) {
                foreach($data['tags'] as $tag) {
                    $new_tag = TagModel::create([
                        'name' => $tag['name']
                    ]);
                    $m->tags()->save($new_tag);
                }
            }
        }

        // return media
        if(count($media > 1)) return $media;
        else return $media[0];

    }

    // update media
    public function updatePivot($media_id, $site_id)
    {
        Site::find($site_id)->media()->find($media_id)->pivot->update(request()->all());
    }


    /**
     * Remove the specified media from storage through AJAX.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {

        $media = Media::find($id);

        $sizes = [
            'xxs',
            'xs',
            'sm',
            'md',
            'lg',
            'xl',
            'original'
        ];

        // delete images
        foreach ($sizes as $size) {
            if (isset($media->$size)) {
                if (file_exists(public_path() . $media->$size )) {
                    unlink(public_path() . $media->$size);
                }
            }
        }

        // delete tags
        // if (isset($media->tags)) {
        //     foreach($media->tags as $tag) {
        //         TagModel::destroy($tag->id);
        //     }
        // }

        // delete media row
        Media::destroy($id);
    }

    /**
     * Attach media
     */
    public function attach($media_id, $site_id)
    {
        $media = Media::find($media_id);
        Site::find($site_id)->media()->save($media);
        return response([
            'media' => $media,
            'message' => [
                'type' => 'success',
                'body' => 'Media attached.'
            ]
        ], 200);
    }

    /**
     * Detach media
     */
    public function detach($media_id, $site_id)
    {
        Site::find($site_id)->media()->detach($media_id);
        return response([
            'message' => [
                'type' => 'success',
                'body' => 'Media detached.'
            ]
        ], 200);
    }

}
