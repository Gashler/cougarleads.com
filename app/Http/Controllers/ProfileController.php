<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index()
    {
        if (auth()->user()->hasRole(['admin'])) {
            return Profile::all();
        } else {
            $with = request('with');
            if (!isset($with)) {
                $with = [];
            };
            $profiles = Profile::with($with)->where('user_id', auth()->user()->id)->get();

            // prepare data for selection on campaign form
            if (isset($with) && $with == 'sites') {
                foreach ($profiles as $index => $profile) {
                    $array = [
                        'id' => null,
                        'title' => $profile->website
                    ];
                    $array2 = $profiles[$index]->sites->toArray();
                    array_unshift($array2, $array);
                    unset($profiles[$index]->sites);
                    $profiles[$index]->sites = $array2;
                }
            }

            return $profiles;
        }
    }

    public function show($key)
    {
        $profile = Profile::find($key);
        if (!$profile) {
            $profile = Profile::where('key', $key)->first();
        }
        return $profile;
    }

    public function public($key)
    {
        $profile = Profile::where('key', $key)->first();
        session(['profile' => $profile]);
        $img = [];
        foreach ($profile->media as $media) {
            if ($media->pivot->key !== '') {
                $img[$media->pivot->key] = $media;
            }
        }
        $profile->img = $img;
        return view('profiles.show', compact('profile'));
    }

    public function store()
    {
        $data = [
            'name' => 'My Company',
            'website' => 'http://mycompany.com'
        ];
        if (auth()->user()->hasRole(['member'])) {
            $data['user_id'] = auth()->user()->id;
        }
        $profile = Profile::create($data);
        $profile->key = $profile->id;
        $profile->update();
        return Profile::find($profile->id);
    }

    public function update($id)
    {
        $data = request()->all();
        Profile::find($id)->update(request()->all());
        return response([
            'message' => [
                'type' => 'success',
                'body' => 'Profile updated.'
            ]
        ], 200);
    }

    public function destroy($id)
    {
        Profile::destroy($id);
        return response([
            'message' => [
                'type' => 'success',
                'body' => 'Profile deleted.'
            ]
        ], 200);
    }
}
