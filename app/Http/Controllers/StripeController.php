<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\StripeService;

class StripeController extends Controller
{
    public function __construct(StripeService $stripeService)
    {
        $this->stripeService = $stripeService;
    }

    /**
    * Return a campaign
    * @param int $user_id
    * @return Response
    */
    public function update($account_id)
    {
        if (auth()->user()->hasRole(['Admin']) || auth()->user()->account_id == $account_id) {
            return $this->stripeService->update(request()->all());
        } else {
            return [
                'error' => true,
                'message' => "You don't have permission to do this."
            ];
        }
    }

    /**
    * Cancel a campaign
    * @param int $account_id
    * @return Response
    */
    public function toggle()
    {
        return $this->stripeService->toggle(request()->all());
    }

    /**
    * Add Payment Method
    * @param int $user_id
    * @return Response
    */
    public function addPaymentMethod()
    {
        return $this->stripeService->addPaymentMethod(request()->all());
    }

    /**
    * Change a campaign's plan
    * @param array $params
    * @return Response
    */
    public function changePlan()
    {
        return $this->stripeService->changePlan(request()->all());
    }
}
