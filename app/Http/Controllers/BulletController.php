<?php

namespace App\Http\Controllers;

use App\Bullet;
use Illuminate\Http\Request;

class BulletController extends Controller
{
    public function store()
    {
        return Bullet::create(request()->all());
    }

    public function update()
    {
        $data = request()->all();
        $bullet = Bullet::find($data['id'])->update($data);
        return response([
            'message' => [
                'type' => 'success',
                'body' => 'Bullet updated'
            ]
        ], 200);
    }

    public function destroy($id)
    {
        Bullet::find($id)->delete();
    }
}
