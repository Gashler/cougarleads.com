<?php

namespace App\Http\Controllers;

use App\Campaign;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Services\StripeService;
use Illuminate\Http\Request;

class CampaignController extends Controller
{
    public function __construct(StripeService $stripeService)
    {
        $this->stripeService = $stripeService;
    }

    /**
    * Get campaigns
    * @return Response
    */
    public function index()
    {
        return auth()->user()->campaigns;
    }

    /**
    * Store a campaign
    * @return Response
    */
    public function store()
    {
        $params = request()->all();
        $stripe_data = $this->stripeService->createSubscription(
            null,
            $params['campaign']['amount'],
            $params['stripe_token']
        );

        // create or update subscription object
        $data = [
            'active' => true,
            'amount' => $params['campaign']['amount'],
            'stripe_subscription_id' => $stripe_data['subscription_id'],
            'stripe_plan_id' => $stripe_data['plan_id'],
            'user_id' => auth()->user()->id
        ];
        $campaign = Campaign::create($data);

        return response([
            'message' => [
                'type' => 'success',
                'body' => 'Success! Your plan is now active.'
            ],
            'campaign' => $campaign
        ], 200);
    }

    /**
    * Return a campaign
    * @param int $account_id
    * @return Response
    */
    public function show($id)
    {
        return Campaign::find($id);
    }

    /**
    * Update a campaign
    * @param int $account_id
    * @return Response
    */
    public function update($id)
    {
        $params = request()->all();
        $campaign = Campaign::find($id);

        // change budget
        if (isset($params['amount'])) {
            $this->stripeService->deleteSubscription($campaign);
            $stripe_plan = $this->stripeService->createPlan($params['amount']);
            $this->stripeService->createSubscription($stripe_plan->id, $params['amount']);
        }

        // activate/deactivate subscription
        if (isset($params['active'])) {
            if ($params['active']) {
                $stripe_data = $this->stripeService->createSubscription(
                    $campaign->stripe_plan_id, $campaign->amount
                );
                $params['stripe_subscription_id'] = $stripe_data['subscription_id'];
                $message = 'Campaign resumed';
            } else {
                $this->stripeService->cancel($campaign->stripe_subscription_id);
                $message = 'Campaign cancelled';
            }
        } else {
            $message = 'Campaign updated';
        }

        $campaign->update($params);
        return response([
            'message' => [
                'type' => 'success',
                'body' => $message
            ],
            'campaign' => Campaign::find($campaign->id)
        ], 200);
    }
}
