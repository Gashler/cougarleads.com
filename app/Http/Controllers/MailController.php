<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class MailController extends Controller
{
    public function send()
    {
        $data = request()->all();
        $client = session('client');
        unset($data['_token']);
        \Mail::send('emails.new_client', [
            'data' => $data
        ], function ($m) use ($client) {
            $m->from(config('mail.from_email'), config('site.company_name'));
            $m->to(config('mail.admin_email'), config('mail.admin_name'))->subject($client->name . ' Quote Request');
        });
        return redirect('contact-confirmation');
    }

    public function contactConfirmation()
    {
        $client = session('client');
        return view('clients.confirmation', compact('client'));
    }
}
