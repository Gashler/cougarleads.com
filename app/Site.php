<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    protected $fillable = [
        'title',
        'key',
        'tagline',
        'description',
        'hours_description',
        'profile_id',
        'services_description',
        'user_id'
    ];

    public function bullets()
    {
        return $this->hasMany(Bullet::class)->where('section', 'bullets');
    }

    public function services()
    {
        return $this->hasMany(Bullet::class)->where('section', 'services');
    }

    public function hours()
    {
        return $this->hasMany(Bullet::class)->where('section', 'hours');
    }

    public function media()
    {
        return $this->belongsToMany(Media::class)->withPivot([
            'key',
            'text'
        ]);
    }

    public function profile()
    {
        return $this->belongsTo(Profile::class);
    }
}
