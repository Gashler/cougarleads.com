<?php

namespace App\Services;

use \Carbon\Carbon;
use \Log;
use App\Services\MailService;
use App\Campaign;
use App\User;

class StripeService
{
    public function __construct(
        MailService $mailService
    ) {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $this->mailService = $mailService;
    }

    /**
    * @param array $params
    * @return Response
    */
    public function createPlan($amount)
    {
        return \Stripe\Plan::create([
            "amount" => $amount * 100,
            "interval" => "week",
            "name" => config('site.app_name') . ' ' . auth()->user()->id . time(),
            "currency" => "usd",
            "id" => auth()->user()->id . time()
        ]);
    }

    /**
    * Create a Subscription
    *
    * @param array $params
    * @return Response
    */
    public function createSubscription($stripe_plan_id, $amount, string $token = null)
    {
        if (!isset($stripe_plan_id)) {
            $stripe_plan = $this->createPlan($amount);
            $stripe_plan_id = $stripe_plan->id;
        }

        // create Stripe customer
        if (!isset(auth()->user()->stripe_id)) {
            $stripe_customer = \Stripe\Customer::create([
                'source' => $token,
                'metadata' => ['user_id' => auth()->user()->id]
            ]);
            auth()->user()->update([
                'stripe_id' => $stripe_customer->id
            ]);
        }

        // create stripe subscription
        $stripe_subscription = \Stripe\Subscription::create([
            "customer" => auth()->user()->stripe_id,
            "items" => [["plan" => $stripe_plan_id]]
        ]);

        return [
            'subscription_id' => $stripe_subscription->id,
            'plan_id' => $stripe_plan_id,
        ];
    }

    /**
    * Return a Subscription
    *
    * @return Response
    */
    public function get()
    {
        if ($stripe_id = auth()->user()->stripe_id) {
            $customer = \Stripe\Customer::retrieve($stripe_id);
            return [
                'error' => false,
                'data' => $customer
            ];
        } else {
            return [
                'error' => true,
                'message' => "You don't currently have a subscription."
            ];
        }
    }

    /**
    * Update a Stripe object property
    *
    * @return Response
    */
    public function update($key, $object_name, $object_id, $value)
    {
        $class = '\Stripe\\' . ucfirst($object_name);
        $object = $class::retrieve($object_id);
        $object->$key = $value;
        $object->save();
        return [
            'error' => false,
            'message' => "Your change has been saved."
        ];
    }

    /**
    * Delete Subscription
    */
    public function deleteSubscription($campaign)
    {
        $stripe_subscription = \Stripe\Subscription::retrieve($campaign->stripe_subscription_id);
        return $stripe_subscription;
        $stripe_subscription->cancel();
        $stripe_plan = \Stripe\Plan::retrieve($campaign->stripe_plan_id);
        $stripe_plan->delete();
    }

    /**
    * Cancel or Activate a Subscription
    *
    * @param array $params
    * @return Response
    */
    public function cancel($subscription_id)
    {
        $subscription = \Stripe\Subscription::retrieve($subscription_id);
        $subscription->cancel();
    }

    /**
    * Add a payment method
    *
    * @param array $params
    * @return Response
    */
    public function addPaymentMethod($params)
    {
        $stripe_id = auth()->user()->stripe_id;
        $customer = \Stripe\Customer::retrieve($stripe_id);
        $source = $customer->sources->create(array("source" => $params['stripeToken']));
        $customer->default_source = $source->id;
        $customer->save();
        return [
            'error' => false,
            'message' => "Payment method added",
            'data' => \Stripe\Customer::retrieve($stripe_id)
        ];
    }

    /**
    * Change a subscription plan
    *
    * @param array $params
    * @return Response
    */
    public function changePlan($params)
    {
        auth()->user()->subscription('main')->swap($params['plan']);
        return [
            'error' => false,
            'message' => "Your subscription plan has been changed.",
            'data' => \Stripe\Customer::retrieve(auth()->user()->stripe_id)
        ];
    }
}
