angular.module('app')
.directive('clMedia', function() {
    return {
        templateUrl : '/components/media/edit.html',
        controller: function($scope, $http) {

            $scope.library = 0;

            $scope.keys = [
                'logo',
                'trust-symbol-1',
                'trust-symbol-2',
                'trust-symbol-3',
                'photo-1',
                'photo-2',
                'photo-3',
            ];

            $scope.mediaTabs = [
                {
                    name: 'Library',
                    library: 1
                },
                {
                    name: 'Clients',
                    library: 0
                }
            ];
            $scope.mediaTab = $scope.mediaTabs[0];

            // toggle media form
            $scope.mediaForm = function()
            {
                $scope.togglePopup('#mediaForm');
            }

            // upload media
            $scope.uploadMedia = function()
            {
                $scope.loading_media = true;

                // prepare data container
                var form_data = new FormData();

                // prepare attachment data
                form_data.append('model', 'App\\Site');
                form_data.append('id', $scope.site.id);
                form_data.append('library', $scope.library);

                // prepare tags
                // tags = [{'name' : 'Games'}];
                // $(tags).each(function(key, tag) {
                //     form_data.append('tags[][name]', tag.name);
                // });

                // prepare files
                files = $('#files')[0].files;
                $(files).each(function(key, file) {
                    form_data.append('files[]', file);
                });

                // send data
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "post",
                    url: "/media",
                    data: form_data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(files) {
                        angular.forEach(files, function(file) {
                            file.key = '';
                            file.text = '';
                            $scope.site.media.push(file);
                            $scope.$apply();
                        });
                        $scope.togglePopup('#mediaForm');
                        $scope.loading_media = false;
                    }
                });
            }

            // toggle media library
            $scope.mediaLibrary = function(toggle)
            {
                if (toggle == undefined) {
                    toggle = true;
                }
                $scope.loading_media = true;
                $scope.selected = null;

                $http.get('/media?type=Image&take=20&library=' + $scope.mediaTab.library).then(function(response) {
                    $scope.loading_media = false;
                    $scope.medias = response.data;
                });
                if (toggle) {
                    $scope.togglePopup('#mediaLibrary');
                }
            }

            $scope.setSelected = function(media, index)
            {
                $scope.selected = media;
                $scope.selected.index = index;
            }

            $scope.setMediaTab = function(tab)
            {
                $scope.mediaTab = tab;
                $scope.mediaLibrary(false);
            }

            // insert media
            $scope.insertMedia = function()
            {

                // ensure that this media isn't already in list
                duplicate = false;
                angular.forEach($scope.site.media, function(media) {
                    if (media.id == $scope.selected.id) {
                        duplicate = true;
                    }
                });

                // attach media to object
                if(!duplicate) {
                    $http.get('/media/' + $scope.selected.id + '/attach/' + $scope.site.id).then(function(response) {
                        if (response.status == 200) {
                            response.data.media.key = '';
                            response.data.media.text = '';
                            $scope.site.media.push(response.data.media);
                        }
                    });
                }

                $scope.togglePopup('#mediaLibrary');
            }

            $scope.updateMediaPivot = function(media)
            {
                $http.put('/media/' + media.id + '/pivot/' + $scope.site.id, media.pivot).then(function(response) {
                    console.log('response = ', response.data);
                });
            }

            // detach media
            $scope.detach = function(media, index)
            {
                $http.get('/media/' + media.id + '/detach/' + $scope.site.id).then(function(response) {
                    if (response.status == 200) {
                        $scope.site.media.splice(index, 1);
                    }
                });
            }

            // detach media
            $scope.deleteMedia = function()
            {
                $http.delete('/media/' + $scope.selected.id).then(function(response) {
                    if (response.status == 200) {
                        $scope.medias.splice($scope.selected.index, 1);
                    }
                });
            }
        }
    }
});
