// routes
angular.module('app').config(function($routeProvider) {
    $routeProvider.

    // dashboard
    when('/', { templateUrl: '/modules/dashboard/index.html', controller:'DashboardController'}).

    // payment
    when('/payment', { templateUrl: '/modules/payment/edit.html', controller:'PaymentController'}).

    // profiles
    when('/profiles', { templateUrl: '/modules/profiles/index.html', controller:'ProfileIndexController'}).
    when('/profiles/:id/edit', { templateUrl: '/modules/profiles/edit.html', controller:'ProfileEditController' }).
    when('/profiles/create', { templateUrl: '/modules/profiles/create.html', controller:'ProfileCreateController' }).

    // sites
    when('/sites', { templateUrl: '/modules/sites/index.html', controller:'SiteIndexController'}).
    when('/sites/:id/edit', { templateUrl: '/modules/sites/edit.html', controller:'SiteEditController' }).

    // campaigns
    when('/campaigns', { templateUrl:'/modules/campaign/index.html', controller:'CampaignIndexController' }).
    when('/campaigns/create/', { templateUrl:'/modules/campaign/create.html', controller:'CampaignCreate' }).
    when('/campaigns/:id/edit', { templateUrl:'/modules/campaign/edit.html', controller:'CampaignEdit' }).

    // users
    when('/users', { templateUrl: '/modules/users/index.html', controller:'SiteIndexController'}).
    when('/users/:id/edit', { templateUrl: '/modules/users/edit.html', controller:'SiteEditController' }).

    // home
    otherwise({ redirectTo: '/' });
})
