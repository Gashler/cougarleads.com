
angular.module('app')
.controller('BaseController', function($scope, $http, $location, $timeout, $interval, $attrs, $window, vars) {

    /****************************
    * Define Common Variables
    *****************************/

    $scope.tab = {
        name: 'Dashboard'
    };

    // $scope.registering = false;
    $scope.baseReady = false;
    $scope.errors = [];

    // get user and config variables
    $scope.getVars = function() {
        vars.async().then(function(result) {
                angular.forEach(result, function(value, key) {
                $scope[key] = value;
            });
            $scope.baseReady = true;
            $scope.authorize();
        });
    };
    if ($scope.user == undefined) {
        $scope.getVars();
    }

    // define variables
    $scope.app_name = app_name;
    $scope.app_url = app_url;
    $scope.messages = [];

    /*************************
    * Messages
    **************************/

    // set message
    $scope.message = function(type, message, timeout)
    {
        // make sure message isn't already being displayed
        angular.forEach($scope.messages, function(message, key) {
            if (message.data == message) {
                return;
            }
        });

        if (!Array.isArray(message)) {
            message = [message];
        }
        $scope.messages.push({
            type: type,
            data: message
        });
        var length = $scope.messages.length;
        $timeout(function() {
            $('.alert-main-container > div:nth-child(' + length + ')').addClass('slideUp');
        }, 10);
        if (timeout) {
            $timeout(function() {
                $('.alert-main-container > div:nth-child(' + length + ')').addClass('fadeOut', function() {
                    $timeout(function() {
                        $scope.messages.splice(length - 1, 1);
                    }, 500);
                });
            }, timeout);
        }
    }

    // set message
    $scope.dismissMessage = function(index)
    {
        $('.alert-main-container > div:nth-child(' + (index + 1) + ')').addClass('fadeOut', function() {
            $timeout(function() {
                $scope.messages.splice(index, 1);
            }, 500);
        });
    }

    // dismiss all messages
    $scope.dismissMessages = function()
    {
        angular.forEach($scope.messages, function(value, index) {
            $scope.dismissMessage(index);
        });
    }

    /*****************
    * Popups
    ******************/
    $scope.changed_unit = false;
    $scope.animating = false;
    $scope.togglePopup = function(popup, changed_unit) {
        console.log('popup = ', popup);
        // initialize variables
        $scope.animating = false;

        // indidcate whether the primary object has been changed
        if (changed_unit == undefined) {
            $scope.changed_unit = true;
        } else {
            $scope.changed_unit = false;
        }

        // set parent
        if ($scope.grandparent !== undefined && $('[data-model="' + $scope.unit.model + '"]').length > 0) {
            var parent = '[data-model="' + $scope.grandparent.model + '"]';
        } else {
            var parent = 'body';
        }

        // show popup
        if ($(popup).css('display') === 'none') {
            // remove placeholders
            $(popup + ' .placeholder').remove();
            // fade in popup
            $(popup).fadeIn(250);
            $(popup + ' .panel').css('margin-top', '20px');
        } else {
            $scope.hidePopup(popup);
        }

        // clicking in black filter
        $('.popup').click(function(event) {
            event.stopPropagation();
            if (!$scope.animating) {
                $scope.animating = true;
                $scope.hidePopup('#' + $(this).attr('id'));
                $scope.$apply();
            }
        });
        $('.popup .popup').click(function(event) {
            event.stopPropagation();
        });

        // prevent clicking on popup from hiding popup and prevent mulitple events triggered by other popups
        id = popup.replace('#', '');
        $(popup + ' .panel, .popup[id!="' + id + '"]', parent).click(function(event) {
            event.stopPropagation();
        });

    }


    // hide popup
    $scope.hidePopup = function(popup) {
        $scope.animating = true;
        $(popup).css('display', 'none !important');
        $(popup + ' .panel').parent().fadeOut(250, function() {
            $timeout(function() {
                $scope.animating = false;
            }, 250);
        });
        $(popup + ' .panel').css('margin-top', '-200px');
    }

    // hide all popups
    $scope.hidePopups = function()
    {
        $scope.hidePopup('.popup');
    }
});
