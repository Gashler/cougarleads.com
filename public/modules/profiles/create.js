angular.module('app').controller('ProfileCreateController', function($scope, $http, $location, $routeParams) {
    $http.post('/profiles').then(function(response) {
        $scope.profile = response.data;
        $scope.user.has_profile = true;
        $location.path('/profiles/' + $scope.profile.id + '/edit');
    });
});
