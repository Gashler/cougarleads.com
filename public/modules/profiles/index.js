angular.module('app').controller('ProfileIndexController', function($scope, $http, $location) {

    $scope.tab.name = 'Company Profiles';

    // create profile
    $scope.getProfiles = function()
    {
        $scope.loading = true;
        $http.get('/profiles').then(function(response) {
            $scope.loading = false;
            $scope.profiles = response.data;
        });
    }
    $scope.getProfiles();

    $scope.deleteProfile = function(profile, index)
    {
        if (confirm('Are you sure you want to delete this company profile?')) {
            $http.delete('/profiles/' + profile.id).then(function(response) {
                if (response.status == 200) {
                    $scope.profiles.splice(index, 1);
                }
            });
        }
    }
});
