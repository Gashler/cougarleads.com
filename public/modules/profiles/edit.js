angular.module('app').controller('ProfileEditController', function($scope, $http, $location, $routeParams) {

    $scope.tab.name = 'Company Profiles';

    $scope.tabs = [
        'Basic',
        'Address'
    ];
    $scope.tab = $scope.tabs[0];

    $scope.selectTab = function(tab)
    {
        $scope.tab = tab;
    }

    $scope.getProfile = function()
    {
        $scope.loading = true;
        $http.get('/profiles/' + $routeParams.id).then(function(response) {
            $scope.loading = false;
            $scope.profile = response.data;
            console.log('response.data = ', response.data);
        });
    }
    $scope.getProfile();

    // patch profile
    $scope.patch = function(key, value)
    {
        $http.patch('/profiles/' + $scope.profile.id, {
            [key]: value
        }).then(function(response) {
            console.log('response = ', response.data);
        });
    }

    $scope.createBullet = function(section)
    {
        $http.post('/bullets', {
            profile_id: $scope.profile.id,
            section: section
        }).then(function(response) {
            console.log('response.data = ', response.data);
            $scope.profile[section].push(response.data);
        });
    }

    // edit bullet
    $scope.updateBullet = function(bullet)
    {
        $http.patch('/bullets/' + bullet.id, bullet).then(function(response) {
            console.log('response = ', response.data);
        });
    }

    // delete bullet
    $scope.deleteBullet = function(bullet, section, $index)
    {
        $http.delete('/bullets/' + bullet.id).then(function(response) {
            if (response.status == 200) {
                $scope.profile[section].splice($index, 1);
            }
        })
    }
});
