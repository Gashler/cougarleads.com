angular.module('app').controller('DashboardController', function($scope, $http, $location) {

    $scope.tab.name = 'Dashboard';

    // create site
    $scope.getStats = function()
    {
        $scope.loadingStats = true;
        $http.get('/stats').then(function(response) {
            $scope.loadingStats = false;
            $scope.stats = response.data;
        });
    }
    $scope.getStats();
});
