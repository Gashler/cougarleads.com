angular.module('app')
.controller('CampaignCreate', function($scope, $http, $location, $routeParams, $filter, $timeout) {

    $scope.tab.name = 'Campaigns';

    $scope.campaign = {};
    $scope.payment = {};

    // initialize billing months array
    $scope.months = [];
    for (month = 1; month <= 12; month ++) {
        if (month < 10) {
            month = '0' + month;
        }
        $scope.months.push(month);
    }

    // initialize billing years array
    $scope.years = [];
    var year = parseInt(new Date().getFullYear());
    var max = year + 11;
    for (year; year < max; year ++) {
        $scope.years.push(year);
    }

    // get profiles
    $scope.getProfiles = function()
    {
        $http.get('/profiles?with=sites').then(function(response) {
            $scope.profiles = response.data;
            $scope.profile = $scope.profiles[0];
            $scope.site = $scope.profiles[0].sites[0];
        });
    }
    $scope.getProfiles();

    // get token
    $scope.getToken = function()
    {
        $scope.loading = true;
        Stripe.card.createToken($scope.payment, createCampaign);
    }

    // Stripe Response Handler
    function createCampaign(code, result)
    {
        if (result.error) {
            $scope.loading = false;
            $scope.message('danger', result.error.message, 10000);
        } else {

            // prepare data
            $scope.campaign.profile_id = $scope.profile.id;
            if (!$scope.campaign.landing_page) {
                $scope.campaign.site_id = $scope.site.id;
            }

            $http.post('/campaigns', {
                stripe_token: result.id,
                campaign: $scope.campaign
            }).then(function(response) {
                $scope.loading = false;
                if (!$scope.registering) {
                    $scope.message(response.data.message.type, response.data.message.body, null, 99);
                }
                if (response.data.message.type == 'success') {
                    $location.path('/campaigns/');
                }
            });
        }
        $scope.$apply();
    };
});
