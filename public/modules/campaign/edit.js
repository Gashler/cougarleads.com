angular.module('app')
.controller('CampaignEdit', function($scope, $http, $location, $routeParams, $filter) {

    $scope.tab.name = 'Campaigns';
    $scope.loading = true;
    $scope.loading_toggle = false;
    $scope.loading_payment_method = false;
    $scope.popup_alert = null;
    $scope.statuses = [
        {
            name: 'Active',
            value: 1
        },
        {
            name: 'Paused',
            value: 0
        },
    ];

    // get profiles
    $scope.getProfiles = function()
    {
        $http.get('/profiles?with=sites').then(function(response) {
            $scope.profiles = response.data;
            $scope.profile = $scope.profiles[0];
            $scope.site = $scope.profiles[0].sites[0];
        });
    }
    $scope.getProfiles();

    // get campaigns
    $scope.getCampaign = function() {
        $http.get('/campaigns/' + $routeParams.id).then(function(response) {
            $scope.loading = false;
            if (response.data.error) {
                $scope.message('danger', response.data.message, 5000);
            } else {
                $scope.campaign = response.data;
            }
        });
    }
    $scope.getCampaign();

    // update customer
    $scope.updateCampaign = function(key, value)
    {
        // prepare data
        if (key == 'profile_id') {
            $scope.campaign.profile_id = $scope.profile.id;
            value = $scope.campaign.profile_id;
        }
        if (key == 'site_id') {
            if (!$scope.campaign.landing_page) {
                $scope.campaign.site_id = $scope.site.id;
                value = $scope.campaign.site_id;
            }
        }

        $http.patch('/campaigns/' + $scope.campaign.id, {
            [key]: value
        }).then(function(response) {
            if (response.data.message.body !== 'Campaign updated') {
                $scope.message(
                    response.data.message.type,
                    response.data.message.body,
                    5000
                );
            }
            $scope.campaign = response.data.campaign;
        });
    }

    // update customer
    // $scope.updateObject = function(object, id, key, value)
    // {
    //     $http.put('/stripe/', {
    //         object: object,
    //         object_id: id,
    //         key: key,
    //         value: value,
    //     }).then(function(response) {
    //         if (response.data.error) {
    //             $scope.message('danger', response.data.message, 5000);
    //         } else {
    //             $scope.message('success', response.data.message, 5000);
    //         }
    //     });
    // }

    // cancel or reactivate campaign
    $scope.toggleCampaign = function(method)
    {
        $scope.loading_toggle = true;
        $http.post('/campaigns/toggle', {
            method: method
        }).then(function(response) {
            $scope.loading_toggle = false;
            if (response.data.error) {
                $scope.message('danger', response.data.message, 5000);
            } else {
                $scope.message('success', response.data.message, 5000);
                $scope.customer = response.data.customer;
            }
        });
    }
});
