angular.module('app').controller('CampaignIndexController', function($scope, $http, $location) {

    $scope.tab.name = 'Campaigns';

    // create campaign
    $scope.getCampaigns = function()
    {
        $scope.loading = true;
        $http.get('/campaigns').then(function(response) {
            $scope.loading = false;
            $scope.campaigns = response.data;
        });
    }
    $scope.getCampaigns();

    $scope.deleteCampaign = function(campaign, index)
    {
        if (confirm('Are you sure you want to delete this campaign?')) {
            $http.delete('/campaigns/' + campaign.id).then(function(response) {
                if (response.status == 200) {
                    $scope.campaigns.splice(index, 1);
                }
            });
        }
    }
});
