angular.module('app').controller('SiteEditController', function($scope, $http, $location, $routeParams) {

    $scope.tab.name = 'Landing Pages';

    $scope.tabs = [
        'Basic',
        'Top Section',
        'Services',
        'Hours',
        'Images',
        'Contact'
    ];
    $scope.tab = $scope.tabs[0];

    $scope.selectTab = function(tab)
    {
        $scope.tab = tab;
    }

    $scope.getProfiles = function()
    {
        $http.get('/profiles').then(function(response) {
            $scope.profiles = response.data;
            console.log('$scope.profiles = ', $scope.profiles);
        });
    }
    $scope.getProfiles();

    $scope.getSite = function()
    {
        $scope.loading = true;
        $http.get('/sites/' + $routeParams.id).then(function(response) {
            $scope.loading = false;
            $scope.site = response.data;
            console.log('response.data = ', response.data);
        });
    }
    $scope.getSite();

    // patch site
    $scope.patch = function(key, value)
    {
        $http.patch('/sites/' + $scope.site.id, {
            [key]: value
        }).then(function(response) {
            console.log('response = ', response.data);
        });
    }

    $scope.createBullet = function(section)
    {
        $http.post('/bullets', {
            site_id: $scope.site.id,
            section: section
        }).then(function(response) {
            console.log('response.data = ', response.data);
            $scope.site[section].push(response.data);
        });
    }

    // edit bullet
    $scope.updateBullet = function(bullet)
    {
        $http.patch('/bullets/' + bullet.id, bullet).then(function(response) {
            console.log('response = ', response.data);
        });
    }

    // delete bullet
    $scope.deleteBullet = function(bullet, section, $index)
    {
        $http.delete('/bullets/' + bullet.id).then(function(response) {
            if (response.status == 200) {
                $scope.site[section].splice($index, 1);
            }
        })
    }
});
