angular.module('app').controller('SiteIndexController', function($scope, $http, $location) {

    $scope.tab.name = 'Landing Pages';

    // create site
    $scope.getSites = function()
    {
        $scope.loading = true;
        $http.get('/sites').then(function(response) {
            $scope.loading = false;
            $scope.sites = response.data;
        });
    }
    $scope.getSites();

    // create site
    $scope.create = function()
    {
        $http.post('/sites').then(function(response) {
            $scope.site = response.data;
            $location.path('/sites/' + $scope.site.id + '/edit');
        });
    }

    $scope.deleteSite = function(site, index)
    {
        if (confirm('Are you sure you want to delete this landing page?')) {
            $http.delete('/sites/' + site.id).then(function(response) {
                if (response.status == 200) {
                    $scope.sites.splice(index, 1);
                }
            });
        }
    }
});
