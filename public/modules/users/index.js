angular.module('app').controller('UserIndexController', function($scope, $http, $location) {

    // create user
    $scope.getUsers = function()
    {
        $scope.loading = true;
        $http.get('/users').then(function(response) {
            $scope.loading = false;
            $scope.users = response.data;
        });
    }
    $scope.getUsers();

    // create user
    $scope.create = function()
    {
        $http.post('/users').then(function(response) {
            $scope.user = response.data;
            $location.path('/users/' + $scope.user.id + '/edit');
        });
    }

    $scope.deleteUser = function(user, index)
    {
        if (confirm('Are you sure you want to delete this user?')) {
            $http.delete('/users/' + user.id).then(function(response) {
                if (response.status == 200) {
                    $scope.users.splice(index, 1);
                }
            });
        }
    }
});
