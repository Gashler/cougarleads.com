angular.module('app').controller('UserEditController', function($scope, $http, $location, $routeParams) {

    $scope.tabs = [
        'Basic',
        'Top Section',
        'Services',
        'Hours',
        'Images',
        'Contact'
    ];
    $scope.tab = $scope.tabs[0];

    $scope.selectTab = function(tab)
    {
        $scope.tab = tab;
    }

    $scope.getUser = function()
    {
        $scope.loading = true;
        $http.get('/users/' + $routeParams.id).then(function(response) {
            $scope.loading = false;
            $scope.user = response.data;
        });
    }
    $scope.getUser();

    // patch user
    $scope.patch = function(key, value)
    {
        $http.patch('/users/' + $scope.user.id, {
            [key]: value
        }).then(function(response) {
            console.log('response = ', response.data);
        });
    }

    $scope.createBullet = function(section)
    {
        $http.post('/bullets', {
            user_id: $scope.user.id,
            section: section
        }).then(function(response) {
            console.log('response.data = ', response.data);
            $scope.user[section].push(response.data);
        });
    }

    // edit bullet
    $scope.updateBullet = function(bullet)
    {
        $http.patch('/bullets/' + bullet.id, bullet).then(function(response) {
            console.log('response = ', response.data);
        });
    }

    // delete bullet
    $scope.deleteBullet = function(bullet, section, $index)
    {
        $http.delete('/bullets/' + bullet.id).then(function(response) {
            if (response.status == 200) {
                $scope.user[section].splice($index, 1);
            }
        })
    }
});
