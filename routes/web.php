<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('test', function() {
    return auth()->user()->role->name;
});

Route::get('/', function() {
    return view('home');
});

Route::resource('sessions', 'SessionController');
Route::get('login', ['as' => 'login', 'uses' => 'SessionController@create']);
Route::get('logout', array('as' => 'logout', 'uses' => 'SessionController@destroy'));
Route::get('sign-up/{code}', array('as' => 'sign-up', 'uses' => 'UserController@create'));
Route::get('password/forgot', 'SessionController@forgotPassword');
Route::post('password/reset', 'SessionController@resetPassword');
Route::get('password/confirm-reset', 'SessionController@confirmResetPassword');

Route::group(array('middleware' => 'auth'), function() {
    Route::get('app', function() {
        return view('layouts.app');
    });
});

Route::resource('profiles', 'ProfileController');
Route::resource('sites', 'SiteController');
Route::get('c/{key}', 'SiteController@public');

Route::resource('bullets', 'BulletController');

Route::resource('media', 'MediaController');
Route::get('media/{media_id}/detach/{site_id}', 'MediaController@detach');
Route::get('media/{media_id}/attach/{site_id}', 'MediaController@attach');
Route::put('media/{media_id}/pivot/{site_id}', 'MediaController@updatePivot');

Route::post('request-contact', 'MailController@send');
Route::get('contact-confirmation', 'MailController@contactConfirmation');

Route::resource('register', 'RegisterController');

Route::resource('stats', 'StatController');

// campaigns
Route::resource('campaigns', 'CampaignController');
Route::post('Campaign/toggle', 'CampaignController@toggle');
Route::post('Campaign/addPaymentMethod', 'CampaignController@addPaymentMethod');
Route::post('Campaign/changePlan', 'CampaignController@changePlan');
Route::get('campaign-plan', 'CampaignController@plan');
Route::get('campaign-plans/{id?}', 'CampaignController@plans');
// Route::post('campaigns/create-plan', 'CampaignController@createPlan');

// Stripe
Route::resource('stripe', 'StripeController');

Route::get('vars', 'Controller@vars');

// Route::get('sites', 'SiteController@index');
// Route::post('sites', 'SiteController@create');
// Route::patch('sites/{id}', 'SiteController@patch');
// Route::get('sites/{key}', 'SiteController@show');
// Route::delete('sites/{key}/delete', 'SiteController@delete');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
//
// Route::get('/home', 'HomeController@index')->name('home');
