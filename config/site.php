<?php

return [
    'title' => env('APP_NAME') . 'The Easiest Way to Get Hot Leads',
    'app_url' => env('APP_URL'),
    'app_name' => env('APP_NAME'),
    'phone' => '(801) 900-3035',
    'email' => 'cougarleads@gmail.com',
];
