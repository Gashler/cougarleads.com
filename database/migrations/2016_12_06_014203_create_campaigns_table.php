<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function ($table) {
            $table->increments('id');
            $table->double('amount');
            $table->boolean('active');
            $table->string('landing_page')->nullable();
            $table->string('name')->nullable();
            $table->integer('user_id');
            $table->string('stripe_subscription_id');
            $table->string('stripe_plan_id');
            $table->timestamp('ends_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
