<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMediaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('media', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('type')->nullable();
			$table->string('url')->nullable();
			$table->string('title')->nullable();
			$table->text('description')->nullable();
			$table->integer('height')->nullable();
			$table->integer('width')->nullable();
			$table->integer('size')->nullable();
			$table->string('extension')->nullable();
			$table->boolean('library')->default(0);
			$table->boolean('disabled')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('media');
	}

}
