<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'first_name' => 'Captain',
                'last_name' => 'Cougar',
                'email' => 'leadcougar@gmail.com',
                'password' => \Hash::make('asdf;lkjasdf;lkj'),
                'role_id' => 2
            ],
            [
                'first_name' => 'John',
                'last_name' => 'Doe',
                'email' => 'jdoe@example.com',
                'password' => \Hash::make('asdf'),
                'role_id' => 1
            ],
        ];
        User::insert($users);
    }
}
